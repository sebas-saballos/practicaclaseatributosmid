import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PrincipalMenu {

	// atributo
	static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	//creando objeto - Este da error desde la creaci�n del constructor 
	// static Camiseta camiseta = new Camiseta();

	 static Camiseta camiseta;
	
	// m�todos
	public static void main(String[] args) throws IOException {

		// Objetos
		menu();

	}

	public static void menu() throws IOException {
		int opcion = 0;
		do {
			System.out.println("*** Bienvenido al sistema Camisola ***");
			System.out.println("1. Registar una camiseta.");
			System.out.println("2. Imprimir camiseta");
			System.out.println("3. Salir");
			System.out.println();
			System.out.print("Ingrese la opci�n: ");
			opcion = Integer.parseInt(in.readLine());
			procesarOpcion(opcion);
		} while (opcion != 3);
	}

	public static void procesarOpcion(int opcionDelMenu) throws IOException {
		switch (opcionDelMenu) {
		case 1:
			registarCamiseta();
			break;
		case 2:
			imprimirCamiseta();
			break;
		case 3:
			System.out.println();
			System.out.println("Gracias por su visita");
			System.exit(0);
		default:
			System.out.println("Opci�n invalida");
		}
	}

	public static void registarCamiseta() throws IOException {
    	// Aqu� no lleva el static
    	//Camiseta camiseta = new Camiseta();
		// camiseta = new Camiseta('S', 14.1500, "Rojo", "Adidas" , "Polo", 0);
        System.out.print("Ingrese la talla: ");
        char talla = in.readLine().charAt(0); 
        System.out.print("Ingrese el precio: ");
        double  precio = Double.parseDouble(in.readLine()) ;
        System.out.print("Ingrese el color: ");
        String color = in.readLine();
        System.out.print("Ingrese la marca: ");
        String marca = in.readLine();
        System.out.print("Ingrese el estilo: ");
        String estilo = in.readLine();  
        System.out.print("Ingrese el tipo: ");
        int tipo = Integer.parseInt(in.readLine());
        System.out.println("");
        System.out.println("*|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|*");
      
       camiseta = new Camiseta(talla,precio,color,marca,estilo,tipo);
	}
	
	public static void imprimirCamiseta() {

	//	camiseta.imprimirCamiseta();
	String datosDeCamiseta= camiseta.toString(); // Cree una variable String para almacenar el ToString
	System.out.println(datosDeCamiseta);
	}
}
