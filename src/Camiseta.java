// PASO 2 CREAR CLASE 
public class Camiseta {
	
	//PASO 3 
	// Definici�n de los atributo

	private char talla;
	private double precio;
	private String color;
	private String marca;
	private String estilo;
	private int tipo; // 1= Manga larga, 2=manga corta


	// Para crear los GET & SET auto click derecho,Generate, Getter & Setter 
	
	public char getTalla() {
		return talla;
	}
	
	public void setTalla(char tallaParametro) {
		this.talla = tallaParametro;
		
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getEstilo() {
		return estilo;
	}

	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	
	public String toString() {
		return "Camiseta [talla=" + talla + ", precio=" + precio + ", color=" + color + ", marca=" + marca + ", estilo="
				+ estilo + ", tipo=" + tipo + "]";
	}
	

	//Creaci�n de constructor 
			// 1- Se llamar� igual que la clase 2- No devuelve nada 
			// 3- Definir parametros que recibir� igual a los atributos
			public Camiseta(char talla, double precio,String color , String marca, String estilo, int tipo) {
				
			this.talla = talla;
			this.precio = precio;
			this.color = color;
			this.marca  = marca ; 
			this.estilo = estilo; 
			this.tipo = tipo;
				
				}

	
	// PASO 4 
	// Definir un M�todo = Rutinas como en el cuatri pasado
	
	
// LOS METODOS LOS VAMOS A MANEJAR PUBLICOS Y LOS ATRIBUTOS PRIVADOS 
//	public void imprimirCamiseta() {
//
//		System.out.println("Imprimiendo el valor de la camiseta");
//		System.out.println("Talla : " + talla);
//		System.out.println("Precio : " + precio);
//		System.out.println("Color : " + color);
//		System.out.println("marca : " + marca);
//		System.out.println("estilo : " + estilo);
//		System.out.println("tipo : " + tipo);
//
//	}
			//Comentario 
	
	
	
	
	
	
	
}